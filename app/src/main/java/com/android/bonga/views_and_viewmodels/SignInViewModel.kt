package com.android.bonga.views_and_viewmodels

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import com.android.bonga.utils.Authentication

class SignInViewModel : ViewModel() {
    fun signInUser(context: Context, email: String, password: String, view: View) {
        Authentication.signInUser(context, email, password, view)
    }
}
