package com.android.bonga.views_and_viewmodels

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.android.bonga.R
import kotlinx.android.synthetic.main.sign_in_fragment.*


class SignInFragment : Fragment() {

    companion object {
        fun newInstance() =
            SignInFragment()
    }

    private lateinit var viewModel: SignInViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.sign_in_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        btn_signin.setOnClickListener {
            try {
                viewModel.signInUser(context!!, et_email_address.text.toString(), et_password.text.toString(), it)
            } catch (e: Exception) {
                Log.d("Error", e.printStackTrace().toString())
            }
        }
        tv_sign_up_link.setOnClickListener {
            it.findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
    }

}
