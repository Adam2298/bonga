package com.android.bonga.views_and_viewmodels

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.bonga.R
import kotlinx.android.synthetic.main.sign_up_fragment.*
import java.lang.Exception


class SignUpFragment : Fragment() {

    companion object {
        fun newInstance() =
            SignUpFragment()
    }

    private lateinit var viewModel: SignUpViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()

        btn_signup.setOnClickListener {
            authenticateAndNavigate(it)
        }

        tv_sign_in_link.setOnClickListener {
            it.findNavController().navigate(R.id.action_signUpFragment_to_signInFragment)
        }
    }

    private fun authenticateAndNavigate(view: View) {
        common_progress_bar.visibility = View.VISIBLE
        try {
            if (et_email_address.text.toString() != "" && et_password.text.toString() != "" && et_confirm_password.text.toString() != "") {
                viewModel.signUpUser(
                    context!!,
                    et_email_address.text.toString(),
                    et_confirm_password.text.toString(),
                    view
                )
                common_progress_bar.visibility = View.GONE
            } else {
                common_progress_bar.visibility = View.GONE
                Toast.makeText(
                    context!!,
                    "Please ensure all fields are filled",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        } catch (e: Exception) {
            Log.d("Error", e.printStackTrace().toString())
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
    }

}
