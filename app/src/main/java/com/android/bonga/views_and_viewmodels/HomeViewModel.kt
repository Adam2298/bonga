package com.android.bonga.views_and_viewmodels

import androidx.lifecycle.ViewModel
import com.android.bonga.utils.Authentication

class HomeViewModel : ViewModel() {
    fun signOutUser() {
        Authentication.signOut()
    }
}
