package com.android.bonga.utils

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.widget.Toast
import androidx.navigation.Navigation
import com.android.bonga.R
import com.android.bonga.components.Components
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.components.Component

// This class will deal with all matters authentication
class Authentication {

    companion object {
        private val authInstance = FirebaseAuth.getInstance()
        private var user: FirebaseUser? = null

        fun createUserAccount(context: Context, email: String, password: String, view: View) {
            authInstance.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                if (it.isSuccessful) {
                    user = authInstance.currentUser
                    user?.sendEmailVerification()
                    signOut()
                    Components.createAlertDialog(
                        context,
                        "Email Verification Link Sent",
                        "Ok",
                        DialogInterface.OnClickListener { _, _ ->
                            Toast.makeText(context, " Message Dismissed", Toast.LENGTH_SHORT).show()
                        })
                    Navigation.findNavController(view)
                        .navigate(R.id.action_signUpFragment_to_signInFragment)
                } else {
                    Components.createAlertDialog(
                        context,
                        "Account Creation failed, please try again",
                        "Ok",
                        DialogInterface.OnClickListener { _, _ ->
                            Toast.makeText(context, " Message Dismissed", Toast.LENGTH_SHORT).show()
                        })
                }
            }
        }

        fun signInUser(context: Context, email: String, password: String, view: View) {
            authInstance.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (checkUserEmailVerified(context)) {
                        Toast.makeText(context, "Sign In Successful", Toast.LENGTH_SHORT).show()
                        Navigation.findNavController(view)
                            .navigate(R.id.action_signInFragment_to_homeFragment)
                    } else {
                        Toast.makeText(context, "Text, please verify your email", Toast.LENGTH_LONG)
                            .show()
                    }
                } else {
                    Toast.makeText(context, "Sign In Failed, please try again", Toast.LENGTH_LONG)
                        .show()
                }
            }
        }

        private fun checkUserEmailVerified(context: Context): Boolean {
            user = authInstance.currentUser
            return if (!user!!.isEmailVerified && user != null) {
                AlertDialog.Builder(context).setMessage("Please verify your email")
                    .setPositiveButton("Ok") { _, _ ->

                    }.create()

                Components.createAlertDialog(context, "Please verify your email", "Ok", DialogInterface.OnClickListener { _, _ ->

                })

                false
            } else {
                true
            }
        }

        fun sendPasswordResetLink(context: Context, email: String) {
            authInstance.sendPasswordResetEmail(email).addOnCompleteListener {
                if (it.isSuccessful) {
                    Toast.makeText(context, "Password reset link sent", Toast.LENGTH_LONG).show()
                    if (authInstance.currentUser != null) {
                        signOut()
                    }
                }
            }
        }

        fun signOut() {
            authInstance.signOut()
        }
    }

}