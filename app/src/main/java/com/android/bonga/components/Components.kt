package com.android.bonga.components

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

class Components {
    companion object {
        fun createNotification() {

        }

        fun createAlertDialog(context: Context, message: String, btnText: String, func: DialogInterface.OnClickListener) {
            AlertDialog.Builder(context).setMessage(message).setPositiveButton(btnText, func).create().show()
        }
    }
}